## **Personal Finance**

A personal finance template with tabs to keep track of:
* monthly transactions
* savings
* budget planning
* paychecks
* 401k
* IRA
* HSA
* stocks
* cash flow

**Sample:** https://docs.google.com/spreadsheets/d/1AhJXyOKEOe00uYJayP3e9Ds0Dmernpzzuyq6tRuceuU/edit?usp=sharing  

**Template:** https://docs.google.com/spreadsheets/d/14Jyx4AL9Ey4VfGPA3Z8m20hINd_RR5BNOJc5EIdbL5U/edit?usp=sharing

## **Getting Started**

Open the template and select `File` > `Make a copy...`

Please read the [setup](https://gitlab.com/raphattack/personalfinance/wikis/setup) instructions from the [wiki](https://gitlab.com/raphattack/personalfinance/wikis/home).