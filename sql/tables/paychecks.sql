create table private.paychecks(
	id serial not null,
	date date not null,
	name text not null,
	federal_taxes numeric(12, 2) not null,
	state_taxes numeric(12, 2) not null,
	k401 numeric(12, 2) not null,
	hsa numeric(12, 2) not null,
	espp numeric(12, 2) not null,
	transportation numeric(12, 2) not null,
	benefits numeric(12, 2) not null,
	other_taxes numeric(12, 2) not null
);
