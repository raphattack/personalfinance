create table private.checking (
  id serial not null,
  date date not null,
  merchant text not null,
  category text not null,
  amount numeric(12, 2) not null,
  card text not null
);
